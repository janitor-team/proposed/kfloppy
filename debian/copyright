Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: kfloppy
Source: https://invent.kde.org/utilities/kfloppy
Upstream-Contact: kde-devel@kde.org

Files: *
Copyright: 2002-2003, Adriaan de Groot <groot@kde.org>
           1997, Bernd Johannes Wuebben <wuebben@math.cornell.edu>
           1998, Bjarni R. Einarsson
           2000-2002, KDE e.v
           1999-2002, Meni Livne <livne@kde.org>
           2004-2005, Nicolas Goutte <goutte@kde.org>
           2000, Thad McGinnis
           2015-2016, Wolfgang Bauer <wbauer@tmo.at>
License: GPL-2+

Files: debian/*
Copyright: 2012 Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>
License: GPL-2+

Files: doc/*
       po/de/docs/*
       po/es/docs/*
       po/et/docs/*
       po/fr/docs/*
       po/gl/docs/*
       po/it/docs/*
       po/nl/docs/*
       po/pt/docs/*
       po/pt_BR/docs/*
       po/ru/docs/*
       po/sr/docs/*
       po/sv/docs/*
       po/uk/*
Copyright: 2002, Adriaan de Groot
           1997-2000, Bernd Johannes Wuebben
           2004-2005, Nicolas Goutte
           2000, Thad McGinnis
           2015-2016, Wolfgang Bauer
License: GFDL-1.2+

Files: format.cpp
       format.h
Copyright: 2002, Adriaan de Groot <groot@kde.org>
           2004-2005, Nicolas Goutte <goutte@kde.org>
           2015-2016, Wolfgang Bauer <wbauer@tmo.at>
License: GPL-2

Files: org.kde.kfloppy.appdata.xml
Copyright: Harald Sitter <sitter@kde.org>
License: CC0-1.0

Files: po/uk/kfloppy.po
Copyright: 2002-2016, This_file_is_part_of_KDE
License: LGPL-2.1+3+KDEeV

License: CC0-1.0
 To the extent possible under law, the author(s) have dedicated all copyright
 and related and neighboring rights to this software to the public domain
 worldwide. This software is distributed without any warranty.
 .
 You should have received a copy of the CC0 Public Domain Dedication along
 with this software. If not, see
 <https://creativecommons.org/publicdomain/zero/1.0/>.
 .
 On Debian systems, the complete text of the CC0 Public Domain Dedication
 can be found in "/usr/share/common-licenses/CC0-1.0".

License: GFDL-1.2+
 Permission is granted to copy, distribute and/or modify this document
 under the terms of the GNU Free Documentation License, Version 1.2
 or any later version published by the Free Software Foundation;
 with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
 A copy of the license is included in the section entitled "GNU
 Free Documentation License".
 .
 On Debian systems, the complete text of the GNU Free Documentation
 License version 1.2 can be found in "/usr/share/common-licenses/GFDL-1.2".

License: GPL-2
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 .
 On Debian systems, the complete text of the GNU General Public License
 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 .
 On Debian systems, the complete text of the GNU General Public License
 can be found in "/usr/share/common-licenses/GPL-2".

License: LGPL-2.1+3+KDEeV
 This file is distributed under the license LGPL version 2.1 or
 version 3 or later versions approved by the membership of KDE e.V.
 .
 On Debian systems, the full text of the GNU Library General Public
 License version 2.1 can be found in the file
 `/usr/share/common-licenses/LGPL-2.1'.
 .
 On Debian systems, the full text of the GNU Library General Public
 License version 3 can be found in the file
 `/usr/share/common-licenses/LGPL-3'.
